﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace battlesea
{
    class Placement
    {
        public static void run()
        {
            Console.CursorVisible = false;
            Console.Clear();

            int[,] field = new int[10, 10]{
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                    { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
                };
            int[] ships = new int[4] { 4, 3, 2, 1 };

            int xField = 10, yField = 10;
            for (int y = 0; y < yField + 2; y++)
            {
                for (int x = 0; x < xField + 2; x++)
                {
                    if ((x < 1 || x > xField) || (y < 1 || y > yField))
                    {
                        Console.SetCursorPosition(x * 2, y);
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        if ((y < 1 || y > yField) && (x >= 1 && x <= xField))
                        {
                            Console.Write(String.Format("{0,-2}", (char)(64 + x)));
                        }
                        else if ((y >= 1 && y <= yField) && (x < 1 || x > xField))
                        {
                            Console.Write(String.Format("{0,-2}", y));
                        }
                        else
                        {
                            Console.Write(String.Format("{0,-2}", ""));
                        }
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                }
            }

            Console.SetCursorPosition((xField + 4) * 2, 7);
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("        ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(" - x1");
            Console.SetCursorPosition((xField + 4) * 2, 5);
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("      ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(" - x2");
            Console.SetCursorPosition((xField + 4) * 2, 3);
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("    ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(" - x3");
            Console.SetCursorPosition((xField + 4) * 2, 1);
            Console.BackgroundColor = ConsoleColor.White;
            Console.Write("  ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(" - x4");

            int substring = Console.WindowWidth - 69;
            if (substring > 0)
            {
                string str;
                Console.SetCursorPosition(50, 2);
                Console.Write(String.Format("{0,-16} - {1}", "Стрелочки", (str = "Перемешение корабля").Substring(0, Math.Min(substring, str.Length))));
                Console.SetCursorPosition(50, 3);
                Console.Write(String.Format("{0,-16} - {1}", "PageUP, PageDown", (str = "Выбор корабля").Substring(0, Math.Min(substring, str.Length))));
                Console.SetCursorPosition(50, 4);
                Console.Write(String.Format("{0,-16} - {1}", "Num0", (str = "Поворот корабля").Substring(0, Math.Min(substring, str.Length))));
                Console.SetCursorPosition(50, 5);
                Console.Write(String.Format("{0,-16} - {1}", "Пробел", (str = "Установить корабль").Substring(0, Math.Min(substring, str.Length))));
            }

            ConsoleKey key;
            int xPos = 5,
                yPos = 5;
            int ship = 0;
            int xRotate = 1, yRotate = 0;
            Console.SetCursorPosition(xPos * 2, yPos);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(String.Format("{0,-" + ((ship + 1) * 2) + "}", ""));
            Console.BackgroundColor = ConsoleColor.Black;

            Console.SetCursorPosition((xField + 4) * 2 - 2, ship * 2 + 1);
            Console.Write('*');
            while (true)
            {
                key = Console.ReadKey().Key;
                Console.SetCursorPosition(xPos * 2, yPos);
                if (xRotate != 0)
                {
                    Console.Write(String.Format("{0,-" + ((ship + 1) * 2) + "}", ""));
                }
                else
                {
                    for (int i = 0; i < ship + 1; i++)
                    {
                        Console.SetCursorPosition(xPos * 2, yPos + i);
                        Console.Write("  ");
                    }
                }
                Console.SetCursorPosition(xPos * 2, yPos);

                Console.SetCursorPosition((xField + 4) * 2 - 2, ship * 2 + 1);
                Console.Write(' ');
                switch (key)
                {
                    case ConsoleKey.LeftArrow:
                        xPos--;
                        break;
                    case ConsoleKey.RightArrow:
                        xPos++;
                        break;
                    case ConsoleKey.UpArrow:
                        yPos--;
                        break;
                    case ConsoleKey.DownArrow:
                        yPos++;
                        break;

                    case ConsoleKey.PageUp:
                        if (ship > 0)
                        {
                            ship--;
                        }
                        break;
                    case ConsoleKey.PageDown:
                        if (ship < 3)
                        {
                            ship++;
                        }
                        break;

                    case ConsoleKey.NumPad0:
                        int t = xRotate;
                        xRotate = yRotate;
                        yRotate = t;
                        break;

                    case ConsoleKey.Enter:
                    case ConsoleKey.Spacebar:
                        if (ships[ship] > 0)
                        {
                            int[,] colision = new int[ship * xRotate + 1 + 2, ship * yRotate + 1 + 2];
                            bool colapse = false;
                            for (int i = 0; i < ship * xRotate + 1 + 2; i++)
                            {
                                for (int j = 0; j < ship * yRotate + 1 + 2; j++)
                                {
                                    try
                                    {
                                        if (field[xPos - 1 + i - 1, yPos - 1 + j - 1] > 0)
                                        {
                                            colapse = true;

                                            break;
                                        }
                                    }
                                    catch (Exception ex) { }
                                }
                                if (colapse)
                                {
                                    break;
                                }
                            }
                            if (!colapse)
                            {
                                ships[ship]--;
                                for (int i = 0; i < ship + 1; i++)
                                {
                                    field[xPos + i * xRotate - 1, yPos + i * yRotate - 1] = 1;
                                }
                            }
                        }
                        break;
                }
                if (xPos <= 0)
                {
                    xPos = 1;
                }
                else if (xPos > (xField - ship * xRotate))
                {
                    xPos = xField - ship * xRotate;
                }
                if (yPos <= 0)
                {
                    yPos = 1;
                }
                else if (yPos > (yField - ship * yRotate))
                {
                    yPos = (yField - ship * yRotate);
                }

                for (int x = 0; x < 10; x++)
                {
                    for (int y = 0; y < 10; y++)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.SetCursorPosition((x + 1) * 2 * field[x, y], (y + 1) * field[x, y]);
                        Console.Write("  ");
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                }

                Console.SetCursorPosition(xPos * 2, yPos);
                Console.BackgroundColor = ConsoleColor.Red;
                if (xRotate != 0)
                {
                    Console.Write(String.Format("{0,-" + ((ship + 1) * 2) + "}", ""));
                }
                else
                {
                    for (int i = 0; i < ship + 1; i++)
                    {
                        Console.SetCursorPosition(xPos * 2, yPos + i);
                        Console.Write("  ");
                    }
                }
                Console.BackgroundColor = ConsoleColor.Black;

                Console.SetCursorPosition((xField + 4) * 2 - 2, ship * 2 + 1);
                Console.Write('*');

                Console.SetCursorPosition((xField + 4 + ship + 1 + 2) * 2, ship * 2 + 1);
                Console.Write(String.Format("{0,-2}", ships[ship]));

                /*for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Console.SetCursorPosition(i, j + 15);
                        Console.Write(field[i,j]);
                    }
                }*/

                Console.SetCursorPosition(xPos * 2, yPos);
                if (ships[0] == 0 && ships[1] == 0 && ships[2] == 0 && ships[3] == 0)
                {
                    Console.SetCursorPosition(0, 15);
                    break;
                }
            }//while
        }
    }
}
